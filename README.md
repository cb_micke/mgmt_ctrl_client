# README #

Example code for connecting to mgmt_ctrl over modbus

### Modbus commands ###

mgmt_ctrl expects either function 0x03 Read holding registers or function 0x10 Preset Multiple Registers on the specified addresses to execute its functions.

| Function | Address  | Register Name | Description | Length |
|----------|----------|---------------|-------------|--------|
| 0x03     | 0x0000   | Identifier    |  Returns the MAC-address of the device as a NULL-terminated string with six octets separated by colon and a newline character. I.e. 11:22:33:44:55:66\n the expected response is 9 registers long | 9 |
| 0x10     | 0x0100   | MainMCU download  | Expects a filename, sent by the Preset Multiple Registers function. Download specified target file name to palette. Errors handled by the error response 0x90 + error code 01, file not found. 02 Other error | |
| 0x10     | 0x0200   | AppMCU download  | Expects a filename, sent by the Preset Multiple Registers function. Download specified target file name to palette. Errors handled by the error response 0x90 + error code 01, file not found. 02 Other error | |
| 0x10     | 0x0300   | ADuC download  | Expects a filename, sent by the Preset Multiple Registers function. Download specified target file name to palette. Errors handled by the error response 0x90 + error code 01, file not found. 02 Other error | |
| 0x03     | 0x1001-0x1010   | UUT present  | UUT presence detection: 0x00 UUT not present 0x01 UUT is present, the address determines the port (1-16) 0x1001 = 1, 0x1010 = 16 | 1 |
| 0x03     | 0x2001-0x2010   | Program Main MCU | Control programming of port 1-16 for main MCU. 0 - Programming succeeded 1 - Programming failed. Target file not downloaded. 2 – Programming failed. Other reason. | 1 |
| 0x03     | 0x2021-0x2030   | Program App MCU | Control programming of port 1-16 for App MCU. 0 - Programming succeeded 1 - Programming failed. Target file not downloaded. 2 – Programming failed. Other reason. | 1 |
| 0x03     | 0x2041-0x2050   | Program ADuC MCU | Control programming of port 1-16 for ADuC. 0 - Programming succeeded 1 - Programming failed. Target file not downloaded. 2 – Programming failed. Other reason. | 1 |
| 0x10     | 0x2101-0x2110   | Set Power | Expects a single register set to 1 or 0, sent by the Preset Multiple Registers function. Set UUT power of port, the address determines the port 1-16 | |


