#!/usr/bin/env python
import time
from timeit import default_timer as timer
import socket

import umodbus
from umodbus import conf
from umodbus.client import tcp

conf.SIGNED_VALUES = True

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# sock.connect(('localhost', 520))
sock.connect(('10.0.20.71', 520))

# Modbus TCP/IP.
# Download main mcu data from tftp
print("Download Main MCU hex bad filename")
payload = 'P1300_G3_3_Rev1_01_aux123_Draft_191230pg.hex1'.encode('ascii')
message = tcp.write_multiple_registers(slave_id=254, starting_address=0x100, values=payload)
error_code = 0
try:
    response = tcp.send_message(message, sock)
except (umodbus.exceptions.IllegalFunctionError, umodbus.exceptions.IllegalDataAddressError) as e:
    error_code = e.error_code
if error_code == 1:
    print('File not found: ' + payload.decode('ascii'))


print("Download Main MCU hex")
payload = 'P1300_G3_3_Rev1_01_aux123_Draft_191230pg.hex'.encode('ascii')
message = tcp.write_multiple_registers(slave_id=254, starting_address=0x100, values=payload)
error_code = 0
try:
    response = tcp.send_message(message, sock)
except (umodbus.exceptions.IllegalFunctionError, umodbus.exceptions.IllegalDataAddressError) as e:
    error_code = e.error_code
if error_code == 1:
    print('File not found: ' + payload.decode('ascii'))


# Download main app data from tftp
print("Download APP MCU hex")
payload = 'P1300_G3_3_Rev1_01_aux123_Draft_191230pg.hex'.encode('ascii')
message = tcp.write_multiple_registers(slave_id=254, starting_address=0x200, values=payload)
error_code = 0
try:
    response = tcp.send_message(message, sock)
except (umodbus.exceptions.IllegalFunctionError, umodbus.exceptions.IllegalDataAddressError) as e:
    error_code = e.error_code
if error_code == 1:
    print('File not found: ' + payload.decode('ascii'))


# Download main aduc data from tftp
print("Download ADuC hex")
payload = 'P915_Rev1_0aux53_181212.hex'.encode('ascii')
message = tcp.write_multiple_registers(slave_id=254, starting_address=0x300, values=payload)
error_code = 0
try:
    response = tcp.send_message(message, sock)
except (umodbus.exceptions.IllegalFunctionError, umodbus.exceptions.IllegalDataAddressError) as e:
    error_code = e.error_code
if error_code == 1:
    print('File not found: ' + payload.decode('ascii'))


# get palette identity
message = tcp.read_holding_registers(slave_id=254, starting_address=0x0000, quantity=9)
response = tcp.send_message(message, sock)
res_data = []
for b in response:
    res_data.append(b.to_bytes(2, byteorder="big"))
print("Palette identity response")
print(res_data)

# Check presence on port 1
message = tcp.read_holding_registers(slave_id=254, starting_address=0x1001, quantity=1)
response = tcp.send_message(message, sock)
res_data = []
for b in response:
    res_data.append(b.to_bytes(2, byteorder="big"))
print("Presence response")
print(res_data)

# Set power 1 on port 1
print("Power on DUT")
message = tcp.write_multiple_registers(slave_id=254, starting_address=0x2101, values=[0x01])
response = tcp.send_message(message, sock)

# Program main mcu 1
print("Start MCU Programming")
start = timer()
message = tcp.read_holding_registers(slave_id=254, starting_address=0x2001, quantity=1)
response = tcp.send_message(message, sock)
res_data = []
for b in response:
    res_data.append(b.to_bytes(2, byteorder="big"))
print("MCU response")

wait_for_programming = True
while wait_for_programming:
    # get status
    message = tcp.read_holding_registers(slave_id=254, starting_address=0x6000, quantity=16)
    response = tcp.send_message(message, sock)
    res_data = []
    for reg in response:
        for b in reg.to_bytes(2, byteorder="big"):
            res_data.append(b)
    if res_data[0] == 0x03 and res_data[1] == 0x02:
        print("Programming MCU Success")
        wait_for_programming = False
    print("status response")
    print(res_data)
    time.sleep(5.0)
print(res_data)
end = timer()
print("MainMCU programming time:", end - start)

# program app mcu
print("Start App MCU Programming")
start = timer()
message = tcp.read_holding_registers(slave_id=254, starting_address=0x2021, quantity=1)
response = tcp.send_message(message, sock)
res_data = []
for b in response:
    res_data.append(b.to_bytes(2, byteorder="big"))
print("MCU response")
print(res_data)
wait_for_programming = True
while wait_for_programming:
    # get status
    message = tcp.read_holding_registers(slave_id=254, starting_address=0x6000, quantity=16)
    response = tcp.send_message(message, sock)
    res_data = []
    for reg in response:
        for b in reg.to_bytes(2, byteorder="big"):
            res_data.append(b)
    if res_data[0] == 0x03 and res_data[1] == 0x05:
        print("Programming MCU Success")
        wait_for_programming = False
    print("status response")
    print(res_data)
    time.sleep(5.0)
print("AppMCU programming time:", end - start)

# Program eeprom and aduc 1
print("Start EEPROM and ADuC  Programming")
start = timer()
message = tcp.read_holding_registers(slave_id=254, starting_address=0x2041, quantity=1)
response = tcp.send_message(message, sock)
res_data = []
for b in response:
    res_data.append(b.to_bytes(2, byteorder="big"))


end = timer()
print("ADUC response")
print("EEPROM and ADuC time:", end - start)
print(res_data)
wait_for_programming = True
while wait_for_programming:
    # get status
    message = tcp.read_holding_registers(slave_id=254, starting_address=0x6000, quantity=16)
    response = tcp.send_message(message, sock)
    res_data = []
    for reg in response:
        for b in reg.to_bytes(2, byteorder="big"):
            res_data.append(b)
    if res_data[0] == 0x03 and res_data[1] == 0x08:
        print("Programming ADuC Success")
        wait_for_programming = False
    print("status response")
    print(res_data)
    time.sleep(5.0)

# Set power 0 on port 1
print("Power off DUT")
message = tcp.write_multiple_registers(slave_id=254, starting_address=0x2101, values=[0x00])
response = tcp.send_message(message, sock)

sock.close()

